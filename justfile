export PATH := "./node_modules/.bin:" + env_var('PATH')
export POSTGRES_PASSWORD := "password"

default:
  @just --list --unsorted

# start docker containers
setup:
  docker run --name mongo -p 27017:27017 -d mongo:6
  docker run --name pg -p 5432:5432 -e POSTGRES_PASSWORD -d postgres:15

# insert data
insert amount="100000":
  tsx bin/insert.ts {{amount}}

# query all inserted documents/rows
query:
  tsx bin/query.ts

# drop created collection/table
cleanup:
  tsx bin/cleanup.ts

# remove docker containers
teardown:
  docker rm $(docker stop mongo pg)

# run everything
full: setup insert query teardown

# print sample generated data
generate amount="10":
  tsx bin/generate.ts {{amount}}

