# MongoDB Benchmark Demo

Benchmark query data from MongoDB vs PostgreSQL.

From my various runs:

×|10k|100k
-|-|-
MongoDB|~300ms|~2000ms
PostgreSQL|~200ms|~1500ms

# Setup

`npm` and `just` is available.
Both do the same thing.

```sh
$ npm install
$ npm run setup
```

# Run

```sh
$ npm run insert
$ npm run query
```

# Cleanup

```sh
$ npm run teardown
```
