export default async function benchmark(fn, ...rest) {
  const start = new Date();
  const ret = await fn(...rest);
  const end = new Date();

  console.error(`${fn.name} took ${end - start} ms`);

  return ret;
}
