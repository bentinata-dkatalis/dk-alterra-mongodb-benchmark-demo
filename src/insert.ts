import { MongoClient } from "mongodb";
import pgPromise from "pg-promise";
import generate from "./generate";
import benchmark from "./benchmark";

async function insertMongo(usersData) {
  const client = new MongoClient("mongodb://127.1:27017");
  await client.connect();

  const db = client.db("test");
  const users = db.collection("users");
  const insertResult = await users.insertMany(usersData);
  console.log(`mongo inserted ${insertResult.insertedCount}`);

  client.close();
}

async function insertPg(usersData) {
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD || "password";
  const pgp = pgPromise();
  const db = pgp(
    `postgres://postgres:${POSTGRES_PASSWORD}@127.1:5432/postgres`
  );

  await db.any(`
  CREATE TABLE IF NOT EXISTS users (
    user_id serial PRIMARY KEY,
    username TEXT
  )`);

  await db.any(`
  CREATE TABLE IF NOT EXISTS pockets (
    pocket_id SERIAL PRIMARY KEY,
    user_id INT NOT NULL REFERENCES users,
    pocket_name TEXT
  )`);

  const userColumnSet = new pgp.helpers.ColumnSet(["username"], {
    table: "users",
  });
  const usersCreate =
    pgp.helpers.insert(usersData, userColumnSet) +
    " RETURNING user_id, username";
  const usersCreated = await db.many(usersCreate);
  console.log(`pg inserted ${usersCreated.length} users`);

  const pocketsData = usersCreated.flatMap((user) =>
    usersData
      .find(({ username }) => user.username === username)
      .pockets.map(({ pocketName }) => ({
        pocket_name: pocketName,
        user_id: user.user_id,
      }))
  );
  const pocketColumnSet = new pgp.helpers.ColumnSet(
    ["user_id", "pocket_name"],
    { table: "pockets" }
  );
  const pocketCreate = pgp.helpers.insert(pocketsData, pocketColumnSet);
  const { rowCount } = await db.result(pocketCreate);
  console.log(`pg inserted ${rowCount} pockets`);

  pgp.end();
}

export default async function insert(count) {
  const fakedUsersData = await generate(count);

  await benchmark(insertMongo, fakedUsersData);
  await benchmark(insertPg, fakedUsersData);
}
