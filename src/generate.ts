import { faker } from "@faker-js/faker";
import benchmark from "./benchmark";

class Pocket {
  pocketName: String;
}

function generatePocket(): Pocket {
  return {
    pocketName: faker.finance.accountName(),
  };
}

class User {
  username: String;
  pockets: Pocket[];
}

function generateUser(): User {
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();
  const username = faker.helpers.unique(faker.internet.userName, [
    firstName,
    lastName,
  ]);

  const pocketCount = Math.ceil(Math.random() * 10);
  const pockets = new Array(pocketCount);
  for (let i = 0; i < pocketCount; i++) {
    pockets[i] = generatePocket();
  }

  return {
    username,
    pockets,
  };
}

function generate(count) {
  const users = new Array(count);

  for (let i = 0; i < count; i++) {
    const user = generateUser();
    users[i] = user;
  }

  return users;
}

export default function main(count) {
  return benchmark(generate, count);
}
