import { MongoClient } from "mongodb";
import pgPromise from "pg-promise";
import benchmark from "./benchmark";

async function queryMongo(query = {}) {
  const client = new MongoClient("mongodb://127.1:27017");
  await client.connect();

  const db = client.db("test");
  const users = await db.collection("users").find(query).toArray();

  client.close();
}

async function queryPg(query) {
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD || "password";
  const pgp = pgPromise();
  const db = pgp(
    `postgres://postgres:${POSTGRES_PASSWORD}@127.1:5432/postgres`
  );

  const users = await db.any(`
  SELECT * FROM users
  JOIN pockets
    ON users.user_id = pockets.user_id
  `);

  pgp.end();
}

async function benchmark(fn) {
  const start = new Date();
  await fn();
  const end = new Date();
  console.log(`${fn.name} took ${end - start} ms`);
}

export default async function query() {
  await benchmark(queryMongo);
  await benchmark(queryPg);
}
