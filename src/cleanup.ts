import { MongoClient } from "mongodb";
import pgPromise from "pg-promise";
import benchmark from "./benchmark";

async function cleanupMongo() {
  const client = new MongoClient("mongodb://127.1:27017");
  await client.connect();

  const db = client.db("test");
  await db
    .collection("users")
    .drop()
    .catch(() => {});

  client.close();
}

async function cleanupPg() {
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD || "password";
  const pgp = pgPromise();
  const db = pgp(
    `postgres://postgres:${POSTGRES_PASSWORD}@127.1:5432/postgres`
  );

  await db.none(`DROP TABLE IF EXISTS pockets`);
  await db.none(`DROP TABLE IF EXISTS users`);

  pgp.end();
}

export default async function cleanup() {
  await benchmark(cleanupMongo);
  await benchmark(cleanupPg);
}
