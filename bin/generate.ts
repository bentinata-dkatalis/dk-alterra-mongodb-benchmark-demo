#!/usr/bin/env tsx

import generate from "../src/generate";

let [count] = process.argv.slice(2);
count = count ? Number(count) : 10;

(async function () {
  const generated = await generate(count);
  console.log(JSON.stringify(generated, null, 2));
})();
