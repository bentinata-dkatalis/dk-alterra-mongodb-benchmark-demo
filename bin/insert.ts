#!/usr/bin/env tsx

import insert from "../src/insert";

let [count] = process.argv.slice(2);
count = count ? Number(count) : 100000;

(async function () {
  await insert(count);
})();
